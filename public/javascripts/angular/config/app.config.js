/**
 * 
 */
(function(angular){
	"use strict";
	
	angular.module('AppModule').run(function ($rootScope, $timeout) {
	  $rootScope.message = null;	
	  $rootScope.$watch('message', function (newVal, oldVal) {
		  if(newVal)
			  $timeout(function(){
					$rootScope.$apply(function(){
						$rootScope.message = null;
					});
				}, 3000);
		  })
		});
	
	angular.module('AppModule').config(function ($httpProvider) {
		  $httpProvider.defaults.headers.common = {};
		  $httpProvider.defaults.headers.post = {};
		  $httpProvider.defaults.headers.put = {};
		  $httpProvider.defaults.headers.patch = {};
	});
})(window.angular);