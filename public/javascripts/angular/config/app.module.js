/**
 * 
 */
(function(angular){
	"use strict";
	
	angular.module('AppModule', ['ngRoute', 'ngAnimate', 'ngFileSaver', 'ngSanitize']);
	
})(window.angular)
