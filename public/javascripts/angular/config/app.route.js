/**
 * 
 */
(function(angular){
	"use strict";
	
	angular.module('AppModule').config(['$routeProvider', '$locationProvider',
	    function($routeProvider, $locationProvider) {
	      $routeProvider
	        .when('/search', {
	          templateUrl: 'pages/search/SearchScreen.html',
	          controller: 'ngSearchCtrl'
	        })
	        .when('/upload', {
	          templateUrl: 'pages/upload/UploadScreen.html',
	          controller: 'ngUploadCtrl'
	        }).otherwise('/search');
	      //$locationProvider.html5Mode(true);
	  }]);
})(window.angular);