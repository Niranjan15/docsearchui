/**
 * 
 */
(function(angular){
	"use strict";

	angular.module('AppModule').service('ngSearchService',['ngHttpService', 'UrlsConst', function(ngHttpService, UrlsConst){
		return {
			doSearch : function(searchText, from, size){
				var data = {"searchText" : searchText, "from" : from, "size" : size}
				return ngHttpService.post(UrlsConst.SEARCH, data);
			}
		}
	}]);

})(window.angular);