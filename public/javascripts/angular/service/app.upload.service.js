/**
 * 
 */
(function(angular){
	"use strict";

	angular.module('AppModule').service('ngUploadService',	['ngHttpService', 'UrlsConst', function(httpService, UrlsConst){
		return{
			doUpload : function(file){
			
				return httpService.uploadFile(file, UrlsConst.UPLOAD);

			},
			doUploadMulti : function(files){
				
				return httpService.uploadFiles(files, UrlsConst.BULK_UPLOAD);

			}
		}
	}]);

})(window.angular);