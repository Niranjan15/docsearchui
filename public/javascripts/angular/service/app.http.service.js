/**
 * 
 */
(function(angular){
	"use script";
	
	angular.module('AppModule').factory('ngHttpService',[ '$http', '$q' , 'UrlsConst' , function( $http, $q, UrlsConst){
		return {
			post : function(url, data){

				var deferred = $q.defer();
				$http.post((UrlsConst.CONTEXT + url), data, {headers : {'Content-Type' : 'application/json'}}).then(function(resp){
					if(resp.data.status == 'success')
					{
						deferred.resolve(resp.data.result);
					}
					else
					{
						deferred.reject(resp.data.result);
					}
				}, function(err){
					deferred.reject(err);
				});
				return deferred.promise;
				
			},
			get : function(url){
				
				var deferred = $q.defer();
				$http.get(UrlsConst.CONTEXT + url, {headers : {'Content-Type' : 'application/json'}}).then(function(resp){
					if(resp.data.status == 'success')
					{
						deferred.resolve(resp.data.result);
					}
					else
					{
						deferred.reject(resp.data.result);
					}
				}, function(err){
					deferred.reject(err);
				});
				return deferred.promise;
				
			},
			uploadFile : function(file, uploadUrl){
				
				var deferred = $q.defer();
				var fd = new FormData();
				fd.append('file', file);
            
				$http.post(UrlsConst.CONTEXT + uploadUrl, fd, {
					transformRequest: angular.identity,
					headers: {'Content-Type': undefined}
				}).then(function(resp){
					if(resp.data.status == 'success')
					{
						deferred.resolve(resp.data.result);
					}
					else
					{
						deferred.reject(resp.data.result);
					}
				}, function(err){
            	   deferred.reject(err);
				});
				return deferred.promise;
				
			},
			uploadFiles : function(files, uploadUrl){
				
				var deferred = $q.defer();
				var fd = new FormData();
				for(var i = 0; i < files.length; i++)
				{
					fd.append('file', files[i]);
				}
            
				$http.post(UrlsConst.CONTEXT + uploadUrl, fd, {
					transformRequest: angular.identity,
					headers: {'Content-Type': undefined}
				}).then(function(resp){
					if(resp.data.status == 'success')
					{
						deferred.resolve(resp.data.result);
					}
					else
					{
						deferred.reject(resp.data.result);
					}
				}, function(err){
            	   deferred.reject(err);
				});
				return deferred.promise;
				
			}
		};
	}]);
	
})(window.angular);