/**
 * 
 */
(function(angular){
	"use strict";
	
	angular.module('AppModule').factory('ngDownloadService', ['ngHttpService', 'UrlsConst', function(ngHttpService, UrlsConst){
		return {
			doDownload : function(fileName){
				return ngHttpService.post(UrlsConst.DOWNLOAD, {"id" : fileName});
			}
		}
	}]);
})(window.angular);