/**
 * 
 */
(function(angular){
	"use strict";
	
	angular.module('AppModule').constant('UrlsConst',{
		"CONTEXT" 		:	"https://adminservices-tct.run.aws-usw02-pr.ice.predix.io/",
		"BULK_UPLOAD"	:	"bulkUpload",
		"UPLOAD" 		: 	"commonsblob",
		"SEARCH" 		: 	"search",
		"DOWNLOAD"		: 	"download",
	});
})(window.angular);