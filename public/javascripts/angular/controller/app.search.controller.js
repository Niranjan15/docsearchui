/**
 * 
 */
(function(angular){
	"use strict";

	angular.module('AppModule').controller('ngSearchCtrl',['$scope', '$rootScope', 'ngSearchService', 'ngDownloadService', 'FileSaver', 'Blob',
		function($scope, $rootScope, ngSearchService, ngDownloadService, FileSaver, Blob){
		
		$scope.searchText = "";
		$scope.from = 0;
		$scope.size = 5;
		
		$scope.doSearch = function(){
			
			ngSearchService.doSearch($scope.searchText, $scope.from, $scope.size).then(function(resp){
				$scope.searchResult = resp;
			}, function(err){
				$rootScope.message = {text : "Search failed", type : "error"};
			});
			
		};
		
		$scope.doDownload = function(id){
			ngDownloadService.doDownload(id).then(function(resp){
				var byteCharacters = atob(resp.fileContent);
				var byteNumbers = new Array(byteCharacters.length);
				for (var i = 0; i < byteCharacters.length; i++) {
				    byteNumbers[i] = byteCharacters.charCodeAt(i);
				}
				var byteArray = new Uint8Array(byteNumbers);
			    var blob = new Blob([byteArray], {type: resp.contentType});
			    FileSaver.saveAs(blob, resp.fileName);
			}, function(err){
				$rootScope.message = {text : "Download failed", type : "error"};
			});
		};

		//$scope.doDownload("INCOME_TAX_CALCULATOR.xls");
	}]);

})(window.angular);