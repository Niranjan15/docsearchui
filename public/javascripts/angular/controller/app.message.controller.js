/**
 * 
 */
(function(angular){
	"use strict";
	
	angular.module("AppModule").controller('ngMsgCtrl', ['$scope', '$rootScope' , '$timeout',
		function( $scope, $rootScope, $timeout ){
		
		$rootScope.message = "";
		
		$rootScope.$watch('message', function(){
			$timeout(function(){
				$rootScope.$apply(function(){
					$rootScope.message = null;
				});
			}, 3000);
		});
		
	}]);
	
})(window.angular);