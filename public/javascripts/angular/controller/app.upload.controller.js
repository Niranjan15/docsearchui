/**
 * 
 */
(function(angular){
	"use strict";

	angular.module('AppModule').controller('ngUploadCtrl', ['$scope', '$rootScope', 'ngUploadService', function($scope, $rootScope, ngUploadService){
		$scope.files = [];
		
		$scope.doUpload = function(){
			if($scope.files.length > 0)
			{
				if($scope.files.length == 1)
				{
					ngUploadService.doUpload($scope.files[0]).then(function(resp)
					{
						$rootScope.message = {text : $scope.files[0].name + " file uploaded", type : "success"};
						$scope.files = [];
					}, function(err)
					{
						$rootScope.message = {text : $scope.files[0].name + " file upload failed", type : "error"};
					});
				}
				else
				{
					ngUploadService.doUploadMulti($scope.files).then(function(resp)
					{
						$rootScope.message = {text : $scope.files.length + " file uploaded", type : "success"};
						$scope.files = [];
					}, function(err)
					{
						$rootScope.message = {text : $scope.files.length + " file upload failed", type : "error"};
					});
				}
			}
			else
			{
				var erroMsg = 'No files selected to upload';
				$rootScope.message = {text : erroMsg, type : "warn"};
			}
				
		};
	}]);

})(window.angular);