/**
 * 
 */
(function(angular){
	"use strict";
	
	angular.module('AppModule').controller('ngHeaderCtrl',['$scope', '$location', '$rootScope', '$timeout', function($scope, $location, $rootScope, $timeout){
		$scope.path = "/search";
		$scope.go = function(path){
			$timeout(function(){ 
				$location.path(path);
				$scope.path = path;
			},1)
		};
		
	}]);
})(window.angular);