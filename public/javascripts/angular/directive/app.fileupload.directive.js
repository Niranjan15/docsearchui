/**
 * 
 */
(function(angular){
	"use strict";
	angular.module('AppModule').directive('fileModel', ['$parse', function($parse){
		return {
            restrict: 'A',
            link: function(scope, element, attrs) {
            	var modelArray = scope.$eval(attrs.fileModel);
               
               element.bind('change', function(){
                  scope.$apply(function(){
                	  for(var i = 0; i < element[0].files.length; i++){
                		  modelArray.push(element[0].files[i]);
                	  }
                  });
               });
            }
         };
	}]);
})(window.angular);